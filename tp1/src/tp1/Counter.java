package tp1;

public class Counter {

	private int comp, perm;
	
	public Counter() {
		this.comp = 0;
		this.perm= 0;
	}
	
	public void incComp() {
		this.incComp(1);
	}
	
	public void incComp(int n) {
		this.comp += n;
	}
	
	public void incPerm() {
		this.incPerm(1);
	}
	
	public void incPerm(int n) {
		this.perm += n;
	}
	
	@Override
	public String toString() {
		return "(" + this.comp + ", " + this.perm + ")";
	}
	
	public void reset() {
		this.comp = 0;
		this.perm = 0;
	}
	
}
