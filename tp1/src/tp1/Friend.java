package tp1;

public class Friend implements Comparable<Friend> {

	private String name;
	private int age;
	
	public Friend(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public int compareTo(Friend other) {
		return this.age - other.age;
	}
	
	@Override
	public String toString( ) {
		return this.name + " (" + this.age + ")";
	}
	
}
