package tp1;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		Friend[] friends = new Friend[] {
			new Friend("Korantin", 18),
			new Friend("Autre", 7),
			new Friend("Test", 91)
		};
		
		System.out.println(Arrays.toString(friends));

		Arrays.sort(friends);
		
		System.out.println(Arrays.toString(friends));
	}
	
}
