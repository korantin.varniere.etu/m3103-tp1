package tp1;

import java.util.Arrays;
import java.util.Random;

/*
 * This file is licensed to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * SDD Seance TP 1 : squelette pour les tris de la séance
 *
 * @author <a href="mailto:Frederic.Guyomarch@univ-lille1.fr">Frédéric Guyomarch</a>
 * IUT-A, Universite de Lille, Sciences et Technologies
 */
public class Sort {

	private static final Random RAND = new Random();
	private static final Counter COUNT = new Counter();

	public static int[] generateRdmIntArray(int n, int min, int max){
		return RAND.ints(min, max).limit(n).toArray();
	}

	public static void insertSort(int[] tab, boolean displayCount) {
		System.out.print("INSERT: ");
		
		COUNT.reset();
		
		for(int i=1; i<tab.length; ++i) {
			int index = indexOfInsertion(tab, tab[i], i);
			insertAndShift(index, i, tab);
		}
	    
	    if (displayCount) {
		    System.out.println(COUNT);
		}
	}
	
	/**
	 * <code>indiceInsertion</code> calcule le point d'insertion de la valeur <code>val</code>
	 * dans le tableau trié <code>tab</code> entre les index 0 et <code>idxMax</code>.	
	 * @param tab tableau trié
	 * @param val valeur à insérer
	 * @param idxMax borne max de la recherche de l'index d'insertion
	 * @return index que prendrai la valeur <code>val</code> dans le tableau
	 */
	private static int indexOfInsertion(int[] tab, int val, int idxMax) {
		int lowIndex = 0;
		int middle = 0;
		int high = idxMax;
		
        while(lowIndex < high) {
            middle = (lowIndex + high)/2;
            COUNT.incComp();
            if (val >= tab[middle]) {
                lowIndex = middle+1;
            }
            else {
                high = middle;
            }
        }
        
		return lowIndex;
	}

	/**
	 * <code>inserer</code> insère à l'index <code>iInsert</code>,
	 * la valeur à l'index <code>iVal</code> dans le tableau <code>tab</code>,
	 * sans changer l'ordre des autres valeur du tableau.
	 * @param tab tableau 
	 * @param iVal index de la valeur à insérer
	 * @param iInsert index d'insertion
	 */
	private static void insertAndShift(int iInsert, int iVal, int[] tab) {
		int tmp = tab[iVal];
		System.arraycopy(tab, iInsert, tab, iInsert+1, iVal-iInsert);
		tab[iInsert] = tmp;
		COUNT.incPerm();
	}

	public static int plusPetit(int[] tab, int beg, int end) {
		int idx = beg;
		for (int i = beg; i < end; i++) {
			if (tab[i] < tab[idx]) {
				idx = i;
			}
		}
		return idx;
	}

	public static void selectSort(int[] tab, boolean displayCount) {
		System.out.print("SELECT: ");
		
		COUNT.reset();
		
		for (int i = 0; i < tab.length - 1; i++) {
			int min = plusPetit(tab, i, tab.length);
			COUNT.incComp();
			swap(tab, i, min);
		}
	    
		if (displayCount) {
		    System.out.println(COUNT);
		}
	}

	public static void printArray(int[] tab){
		System.out.print(Arrays.toString(tab));
	}

	public static void swap(int[] tab, int idx, int idx2){
		int tmp = tab[idx];
		tab[idx] = tab[idx2];
		tab[idx2] = tmp;
		COUNT.incPerm();
	}

	public static void bubbleSort(int[] tab, boolean displayCount){
		System.out.print("BUBBLE: ");
		
		COUNT.reset();
		
		for (int i = 0; i < tab.length-1; i++) {
			for (int j = 0; j < tab.length-1; j++) {
				COUNT.incComp();
				if (tab[j] > tab[j+1]) {
					swap(tab, j, j+1);
				}
			}
		}
	    
		if (displayCount) {
		    System.out.println(COUNT);
		}
	}
	
}